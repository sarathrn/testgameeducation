//
//  ViewController.swift
//  TestGame
//
//  Created by Sattin on 27/11/17.
//  Copyright © 2017 Sattin. All rights reserved.
//

import UIKit
import WebKit

class ViewController: UIViewController, WKNavigationDelegate {
    
    // Objects
    var webView: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        setupWebView()
        loadUrl()
    }
    
    func setupWebView() {
        
        let prefs = WKPreferences()
        prefs.javaScriptEnabled = true
        let config = WKWebViewConfiguration()
        config.preferences = prefs
        
        webView = WKWebView(frame: view.frame, configuration: config)
        webView.navigationDelegate = self
        view.addSubview(webView)
    }
    
    func loadUrl() {
        
        webView.navigationDelegate = self
        let url = URL(string: "https://app.educationgalaxy.com/games/ipadgame.html?retries=3&name=blastoff&rocket=3&level=2&score=0&gameTimer=150")!
        var request = URLRequest(url: url)
        request.cachePolicy = .reloadRevalidatingCacheData
        webView.load(request)
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print(webView.url!)
        
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        
        print(webView.url!)
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        
        print("error  - \(error)")
    }
    
}



